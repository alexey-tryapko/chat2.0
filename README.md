# Chat

## Instalation

### Server

Create .env file and copy the contents from the .env.example file into it.

`cd server/`

`npm i && npm run start`

Server will be available on http://localhost:3000/

### Client 

`cd client/`

`npm i && npm run start`


## Users

**ADMIN**  
```json
{
    "email": "admin@gmail.com",
    "password": "admin",
}
```

**ORDINARY USER**  
```json
{
    "email": "kate@gmail.com",
    "password": "1234",
}
```