import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button, Segment } from 'semantic-ui-react';

import styles from './styles.module.css';

class EditMessage extends React.Component {
    constructor(props) {
        super(props);
        const { message } = props.message;
        this.state = {
            body: message,
        }
    }

    editMessageHandle = () => {
        const { body } = this.state;
        if (!body) return;
        this.props.editMessage({ ...this.props.message, message: body });
        this.closeModal();
    };

    closeModal = () => this.props.history.push('/');

    render() {
        const { body } = this.state;
        return (
            <Modal dimmer="blurring" open onClose={this.closeModal} className={styles.modal}>
                <Modal.Header>
                    <span>Edit Message</span>
                </Modal.Header>
                <Segment>
                    <Form onSubmit={this.editMessageHandle}>
                        <Form.TextArea
                            name="body"
                            value={body}
                            placeholder="Type a message..."
                            onChange={ev => this.setState({ body: ev.target.value })}
                        />
                        <Button floated="right" color="blue" type="submit">Save</Button>
                    </Form>
                </Segment>
            </Modal>
        );
    }
};

EditMessage.propTypes = {
    message: PropTypes.objectOf(PropTypes.any).isRequired,
    editMessage: PropTypes.func.isRequired,
    history: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default EditMessage;