import React from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import {
    Grid,
    Form,
    Button,
    Segment,
} from 'semantic-ui-react';

import styles from './styles.module.css';

class EditUser extends React.Component {
    constructor(props) {
        super(props);
        const user = props.user;
        this.state = {
            ...user,
            isEmailValid: true,
            isPasswordValid: true,
            isNameValid: true,
        }
    }

    validateEmail = () => {
        const { email } = this.state;
        const isEmailValid = !validator.isEmpty(email);
        this.setState({ isEmailValid });
        return isEmailValid;
    };

    validateName = () => {
        const { name } = this.state;
        const isNameValid = !validator.isEmpty(name);
        this.setState({ isNameValid });
        return isNameValid;
    };

    validatePassword = () => {
        const { password } = this.state;
        const isPasswordValid = !validator.isEmpty(password);
        this.setState({ isPasswordValid });
        return isPasswordValid;
    };

    emailChanged = email => this.setState({ email, isEmailValid: true });

    nameChanged = name => this.setState({ name, isNameValid: true });

    avatarChanged = avatar => this.setState({ avatar });

    passwordChanged = password => this.setState({ password, isPasswordValid: true });

    validateForm = () => [
        this.validateEmail(),
        this.validateName(),
        this.validatePassword()
    ].every(Boolean);


    handleClickSave = async () => {
        const { id, email, password, avatar, name } = this.state;
        const valid = this.validateForm();
        if (!valid) {
            return;
        }
        try {
            await this.props.editUser({ email, password, name, avatar, id });
            this.close();
        } catch {
            // TODO: show error
        }
    }

    close = () => this.props.history.push('/users');

    render() {
        const { email, name, password, avatar, isEmailValid, isNameValid, isPasswordValid } = this.state;
        return (
            <Grid textAlign="center" verticalAlign="middle" className="fill">
                <Grid.Column style={{ width: 450 }}>
                    <Form name="userForm" size="large" onSubmit={this.handleClickSave}>
                        <Segment>
                            <Form.Input
                                fluid
                                icon="at"
                                iconPosition="left"
                                placeholder="Email"
                                type="email"
                                error={!isEmailValid}
                                value={email}
                                onChange={ev => this.emailChanged(ev.target.value)}
                                onBlur={this.validateEmail}
                            />
                            <Form.Input
                                fluid
                                icon="male"
                                iconPosition="left"
                                placeholder="Name"
                                type="name"
                                error={!isNameValid}
                                value={name}
                                onChange={ev => this.nameChanged(ev.target.value)}
                                onBlur={this.validateName}
                            />
                            <Form.Input
                                fluid
                                icon="photo"
                                iconPosition="left"
                                placeholder="Avatar"
                                type="avatar"
                                value={avatar}
                                onChange={ev => this.avatarChanged(ev.target.value)}
                            />
                            <Form.Input
                                fluid
                                icon="lock"
                                iconPosition="left"
                                placeholder="Password"
                                type="text"
                                value={password}
                                error={!isPasswordValid}
                                onChange={ev => this.passwordChanged(ev.target.value)}
                                onBlur={this.validatePassword}
                            />
                            <div className={styles.buttonWrapper}>
                                <Button onClick={() => this.close()}>
                                    Cancel
                                </Button>
                                <Button type="submit" color="teal" loading={this.props.loading} primary>
                                    Save
                                </Button>
                            </div>
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>
        );
    }
};

EditUser.propTypes = {
    user: PropTypes.objectOf(PropTypes.any).isRequired,
    editUser: PropTypes.func.isRequired,
    history: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default EditUser;