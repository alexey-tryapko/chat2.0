import React from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import validator from 'validator';

import {
    Grid,
    Header,
    Form,
    Button,
    Segment,
} from 'semantic-ui-react';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isEmailValid: true,
            isPasswordValid: true
        };
    }

    validateEmail = () => {
        const { email } = this.state;
        const isEmailValid = !validator.isEmpty(email);
        this.setState({ isEmailValid });
        return isEmailValid;
    };

    validatePassword = () => {
        const { password } = this.state;
        const isPasswordValid = !validator.isEmpty(password);
        this.setState({ isPasswordValid });
        return isPasswordValid;
    };

    emailChanged = email => this.setState({ email, isEmailValid: true });

    passwordChanged = password => this.setState({ password, isPasswordValid: true });

    validateForm = () => [
        this.validateEmail(),
        this.validatePassword()
    ].every(Boolean);

    handleClickLogin = async () => {
        const { email, password } = this.state;
        const { loading } = this.props;
        const valid = this.validateForm();
        if (!valid || loading) {
            return;
        }
        try {
            await this.props.login({ email, password });
        } catch {
            // TODO: show error
        }
    }

    render() {
        const { isEmailValid, isPasswordValid } = this.state;
        return !this.props.isAuthorized
            ? (
                <Grid textAlign="center" verticalAlign="middle" className="fill">
                    <Grid.Column style={{ width: 450 }}>
                        <Header as="h2" color="teal" textAlign="center">
                            Login to your account
                        </Header>
                        <Form name="loginForm" size="large" onSubmit={this.handleClickLogin}>
                            <Segment>
                                <Form.Input
                                    fluid
                                    icon="at"
                                    iconPosition="left"
                                    placeholder="Email"
                                    type="email"
                                    error={!isEmailValid}
                                    onChange={ev => this.emailChanged(ev.target.value)}
                                    onBlur={this.validateEmail}
                                />
                                <Form.Input
                                    fluid
                                    icon="lock"
                                    iconPosition="left"
                                    placeholder="Password"
                                    type="password"
                                    error={!isPasswordValid}
                                    onChange={ev => this.passwordChanged(ev.target.value)}
                                    onBlur={this.validatePassword}
                                />
                                <Button type="submit" color="teal" fluid size="large" loading={this.props.loading} primary>
                                    Login
                                </Button>
                            </Segment>
                        </Form>
                    </Grid.Column>
                </Grid>
            )
            : (this.props.isAdmin ? <Redirect to="/users" /> : <Redirect to="/"/>)
    }
}

Login.propTypes = {
    isAuthorized: PropTypes.bool,
    isAdmin: PropTypes.bool,
    loading: PropTypes.bool.isRequired,
    login: PropTypes.func.isRequired
};

Login.defaultProps = {
    isAuthorized: false,
    isAdmin: false,
};

export default Login;
