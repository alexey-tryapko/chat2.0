import React from 'react';
import PropTypes from 'prop-types';
import Message from "./../Message";
import moment from 'moment';
import styles from './styles.module.css';
import Separator from './../Separator';

class MessageList extends React.Component {
    dateDifference(first, second) {
        const firstDate = moment(first.split(' ')[0]);
        const secondDate = moment(second.split(' ')[0]);
        return secondDate.diff(firstDate, 'days');
    };
    checkingForSeparation(first, second) {
        const diff = this.dateDifference(first, second);
        return diff ? true : false;
    };

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        const { messages, reactMessage, toggleEditedMessage, deleteMessage, user } = this.props;
        let currDate;
        let nextDate;
        return (
            <div className={styles.messageList}>
                {messages.map((message, i, arr) => {
                    let separation;
                    if (arr[i + 1]) {
                        currDate = message.created_at;
                        nextDate = arr[i + 1].created_at;
                        separation = this.checkingForSeparation(currDate, nextDate);
                    }
                    return (
                        <div key={message.id + 1000} className={styles.messageWrapper}>
                            <Message
                                key={message.id}
                                message={message}
                                isCreator={message.user === user.name}
                                reactMessage={reactMessage}
                                toggleEditedMessage={toggleEditedMessage}
                                deleteMessage={deleteMessage}
                                currentUser={user}
                            />
                            {separation && (
                                <Separator date={currDate} />
                            )}
                        </div>
                    )
                })}
                <div style={{ float: "left", clear: "both" }}
                    ref={(el) => { this.messagesEnd = el; }}>
                </div>
            </div>
        );
    }
}

MessageList.propTypes = {
    messages: PropTypes.arrayOf(PropTypes.object).isRequired,
    reactMessage: PropTypes.func.isRequired,
    toggleEditedMessage: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
};

export default MessageList;