import React from 'react';
import PropTypes from 'prop-types';
import { Button, Image, List } from 'semantic-ui-react'
import styles from './styles.module.css';

const User = props => {
    const { user, toggleEditedUser, deleteUser } = props;
    return (
        <List.Item className={styles.item}>
            <List.Content floated='right'>
                <Button onClick={() => deleteUser(user.id)}>Delete</Button>
                <Button onClick={() => toggleEditedUser(user.id)}>Edit</Button>
            </List.Content>
            <Image avatar src={user.avatar} />
            <List.Content>{user.name}</List.Content>
        </List.Item>
    );
};

User.propTypes = {
    user: PropTypes.object.isRequired,
    toggleEditedUser: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
};

export default User;