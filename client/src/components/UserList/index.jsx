import React from 'react';
import PropTypes from 'prop-types';
import User from './../User';
import { List } from 'semantic-ui-react'

import styles from './styles.module.css';
const UserList = props => {
    const { users, toggleEditedUser, deleteUser } = props;
    return (
        <List divided verticalAlign='middle' className={styles.list}>
            {
                users.map(user => (
                    <User
                        key={user.id}
                        user={user}
                        toggleEditedUser={toggleEditedUser}
                        deleteUser={deleteUser}
                    />
                ))
            }
        </List>
    );
};

UserList.propTypes = {
    users: PropTypes.arrayOf(PropTypes.object).isRequired,
    toggleEditedUser: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
};

export default UserList;