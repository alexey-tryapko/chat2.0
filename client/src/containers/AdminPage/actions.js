import {
    ADD_USER,
    EDIT_USER,
    DELETE_USER,
} from './actionTypes';

export const addUser = ({email, name, password, avatar}) => ({
    type: ADD_USER,
    payload: {
        email,
        name,
        avatar,
        password,
    }
});

export const editUser = user => ({
    type: EDIT_USER,
    payload: {
        user
    }
});

export const deleteUser = id => ({
    type: DELETE_USER,
    payload: {
        id
    }
});