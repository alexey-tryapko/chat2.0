import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import Spinner from './../../components/Spinner';
import UserList from './../../components/UserList';
import { Header, Button } from 'semantic-ui-react';
import { fetchUsers } from './../../routines';
import {
    addUser,
    deleteUser,
} from './actions';

import styles from './styles.module.css';

class AdminPage extends React.Component {
    componentDidMount() {
        this.props.fetchUsers();
    }

    editUser(id) {
        this.props.history.push(`/user/${id}`);
    }

    addUser() {
        this.props.history.push('/user/new')
    }

    render() {
        const { users = [], loading, deleteUser } = this.props
        return (
            <div>
                {
                    loading
                        ? <Spinner />
                        : (
                            <div>
                                <div className={styles.headerWrapper}>
                                    <NavLink exact to="/" className={styles.chatLinkWrapper}>
                                        <Header className={styles.chatLink}>
                                            Go to Chat
                                        </Header>
                                    </NavLink>
                                    <Button
                                        className={styles.addBtn}
                                        onClick={() => this.addUser()}
                                    >
                                        Add user
                                    </Button>
                                </div>
                                <UserList
                                    users={users}
                                    toggleEditedUser={id => this.editUser(id)}
                                    deleteUser={deleteUser}
                                />
                            </div>
                        )
                }
            </div>
        );
    }
};

AdminPage.propTypes = {
    users: PropTypes.arrayOf(PropTypes.object),
    loading: PropTypes.bool.isRequired,
    error: PropTypes.string,
    fetchUsers: PropTypes.func.isRequired,
    addUser: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
};

AdminPage.defaultProps = {
    users: [],
    loading: true,
};

const mapStateToProps = rootState => ({
    users: rootState.adminPage.data,
    loading: rootState.adminPage.loading,
    error: rootState.adminPage.error,
    currentUser: rootState.auth.user,
});

const actions = {
    fetchUsers,
    addUser,
    deleteUser,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminPage);