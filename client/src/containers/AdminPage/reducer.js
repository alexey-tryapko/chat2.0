import { fetchUsers } from './../../routines';

const initialState = {
  data: [],
  loading: false,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case fetchUsers.TRIGGER:
      return {
        ...state,
        loading: true,
      };
    case fetchUsers.SUCCESS:
      return {
        ...state,
        data: action.payload,
      };
    case fetchUsers.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case fetchUsers.FULFILL:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
}