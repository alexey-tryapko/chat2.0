import { fetchUsers } from './../../routines';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as userService from './../../services/userService';
import { ADD_USER, EDIT_USER, DELETE_USER } from './actionTypes';

export function* getUsers() {
    try {
        yield put(fetchUsers.request());
        const response = yield call(userService.getAllUsers);

        yield put(fetchUsers.success(response));
    } catch (error) {
        yield put(fetchUsers.failure(error.message));
    } finally {
        yield put(fetchUsers.fulfill());
    }
}

function* watchGetUsers() {
    yield takeEvery(fetchUsers.TRIGGER, getUsers)
}

export function* addUser(action) {
    try {
        const { name, email, password, avatar } = action.payload;
        yield call(userService.addUser, name, email, password, avatar);
        yield put(fetchUsers());
    } catch (error) {
        console.log(error);
    }
}

function* watchAddUser() {
    yield takeEvery(ADD_USER, addUser)
}

export function* editUser(action) {
    try {
        const { user } = action.payload;

        yield call(userService.editUser, user);
        yield put(fetchUsers());
    } catch (error) {
        console.log(error);
    }
}

function* watchEditUser() {
    yield takeEvery(EDIT_USER, editUser)
}

export function* deleteUser(action) {
    try {
        const { id } = action.payload;

        yield call(userService.deleteUser, id);
        yield put(fetchUsers());
    } catch (error) {
        console.log(error);
    }
}

function* watchDeleteUser() {
    yield takeEvery(DELETE_USER, deleteUser)
}

export default function* usersSaga() {
    yield all([
        watchGetUsers(),
        watchAddUser(),
        watchEditUser(),
        watchDeleteUser(),
    ])
};
