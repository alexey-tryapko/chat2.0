import {
    ADD_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE,
    LIKE_MESSAGE,
} from './actionTypes';

export const addMessage = (user, avatar, data) => ({
    type: ADD_MESSAGE,
    payload: {
        data,
        user,
        avatar,
    }
});

export const editMessage = message => ({
    type: EDIT_MESSAGE,
    payload: {
        message
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const likeMessage = (messageId, userId) => ({
    type: LIKE_MESSAGE,
    payload: {
        messageId,
        userId,
    }
});