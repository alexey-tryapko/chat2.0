import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import Header from './../../components/Header';
import MessageList from './../../components/MessageList';
import MessageInput from './../../components/MessageInput';
import Spinner from './../../components/Spinner';
import { Header as HeaderUI } from 'semantic-ui-react';
import { fetchMessages } from './../../routines';
import {
    addMessage,
    deleteMessage,
    likeMessage,
} from './actions'
import styles from './styles.module.css';

const partial = (fn, ...args) => (...rest) => fn(...args.concat(rest));

class Chat extends React.Component {
    getParticipantsNumber(messages) {
        const participants = new Set(messages.map(item => item.user));
        return participants.size;
    };

    getLastMessageTime(messages) {
        return messages[messages.length - 1]["created_at"];
    };

    componentDidMount() {
        this.props.fetchMessages();
        document.addEventListener('keydown', e => (e.keyCode === 38 && this.handleArrowUpPress()))
    }

    handleArrowUpPress() {
        const { messages, currentUser: { user }, toggleEditedMessage } = this.props;
        const userMessages = messages.filter(message => message.user === user);
        if (userMessages.length) toggleEditedMessage(userMessages.pop().id);
    };

    editMessage(id) {
        this.props.history.push(`/message/${id}`);
    }

    render() {
        const {
            loading,
            messages,
            error,
            addMessage,
            deleteMessage,
            likeMessage,
            ...props
        } = this.props;

        const { name: user, avatar, isAdmin } = props.currentUser;
        return (
            <div className={styles.mainWrapper}>
                {
                    loading
                        ? <Spinner />
                        : (
                            <div className={styles.contentWrapper}>
                                {
                                    isAdmin && (
                                        <NavLink exact to="/users" className={styles.usersLinkWrapper}>
                                            <HeaderUI className={styles.usersLink}>
                                                Go to Users
                                            </HeaderUI>
                                        </NavLink>
                                    )
                                }

                                <Header
                                    chatName={props.chatName}
                                    numberOfParticipants={this.getParticipantsNumber(messages)}
                                    numberOfMessages={messages.length}
                                    lastMessageTime={this.getLastMessageTime(messages)}
                                />
                                <MessageList
                                    messages={messages}
                                    reactMessage={likeMessage}
                                    toggleEditedMessage={id => this.editMessage(id)}
                                    deleteMessage={deleteMessage}
                                    user={props.currentUser}
                                />
                                <MessageInput
                                    addMessage={partial(addMessage, user, avatar)}
                                />
                            </div>
                        )
                }
            </div>
        );
    }
}

Chat.propTypes = {
    messages: PropTypes.arrayOf(PropTypes.object),
    loading: PropTypes.bool.isRequired,
    error: PropTypes.string,
    fetchMessages: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired,
    addMessage: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    chatName: PropTypes.string.isRequired,
    currentUser: PropTypes.objectOf(PropTypes.any).isRequired,
};

Chat.defaultProps = {
    messages: [],
    loading: true,
}

const mapStateToProps = rootState => ({
    messages: rootState.chat.data,
    loading: rootState.chat.loading,
    error: rootState.chat.error,
    chatName: rootState.chat.chatName,
    currentUser: rootState.auth.user,
});

const actions = {
    fetchMessages,
    likeMessage,
    addMessage,
    deleteMessage,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);