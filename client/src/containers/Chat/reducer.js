import { fetchMessages } from './../../routines';

const initialState = {
  data: null,
  loading: false,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case fetchMessages.TRIGGER:
      return {
        ...state,
        loading: true,
      };
    case fetchMessages.SUCCESS:
      return {
        ...state,
        data: action.payload,
      };
    case fetchMessages.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case fetchMessages.FULFILL:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
}