import { fetchMessages } from './../../routines';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as messageService from './../../services/messageService';
import { ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, LIKE_MESSAGE } from './actionTypes';

export function* getMessages() {
    try {
        yield put(fetchMessages.request());
        const response = yield call(messageService.getAllMessages);

        yield put(fetchMessages.success(response.sort(
            (a, b) => new Date(a.created_at) - new Date(b.created_at))
        ));
    } catch (error) {
        yield put(fetchMessages.failure(error.message));
    } finally {
        yield put(fetchMessages.fulfill());
    }
}

function* watchGetMessages() {
    yield takeEvery(fetchMessages.TRIGGER, getMessages)
}

export function* addMessage(action) {
    try {
        const { data, user, avatar } = action.payload;
        yield call(messageService.addMessage, data, user, avatar);
        yield put(fetchMessages());
    } catch (error) {
        console.log(error);
    }
}

function* watchAddMessage() {
    yield takeEvery(ADD_MESSAGE, addMessage)
}

export function* editMessage(action) {
    try {
        const { message } = action.payload;

        yield call(messageService.editMessage, message);
        yield put(fetchMessages());
    } catch (error) {
        console.log(error);
    }
}

function* watchEditMessage() {
    yield takeEvery(EDIT_MESSAGE, editMessage)
}

export function* deleteMessage(action) {
    try {
        const { id } = action.payload;

        yield call(messageService.deleteMessage, id);
        yield put(fetchMessages());
    } catch (error) {
        console.log(error);
    }
}

function* watchDeleteMessage() {
    yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export function* likeMessage(action) {
    try {
        const { messageId, userId } = action.payload;

        yield call(messageService.likeMessage, messageId, userId);
        yield put(fetchMessages());
    } catch (error) {
        console.log(error);
    }
}

function* watchLikeMessage() {
    yield takeEvery(LIKE_MESSAGE, likeMessage);
}

export default function* messagesSaga() {
    yield all([
        watchGetMessages(),
        watchAddMessage(),
        watchEditMessage(),
        watchDeleteMessage(),
        watchLikeMessage(),
    ])
};
