import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import EditMessage from '../../components/EditMessage';
import Spinner from './../../components/Spinner';
import { editMessage } from './../Chat/actions';
import { fetchMessage } from './../../routines';

class MessagePage extends React.Component {
    componentDidMount() {
        this.props.fetchMessage({ id: this.props.match.params.id})
    }

    render() {
        const { message, loading, editMessage, history } = this.props;
        return (
            <div>
                {
                    loading
                        ? <Spinner />
                        : (
                            <EditMessage
                                message={message}
                                editMessage={editMessage}
                                history={history}
                            />
                        )
                }
            </div>
        );
    }
}

MessagePage.propTypes = {
    message: PropTypes.objectOf(PropTypes.any),
    loading: PropTypes.bool.isRequired,
    error: PropTypes.string,
    editMessage: PropTypes.func.isRequired,
    fetchMessage: PropTypes.func.isRequired,
};

MessagePage.defaultProps = {
    message: {},
    loading: true,
};

const mapStateToProps = rootState => ({
    message: rootState.messagePage.message,
    loading: rootState.messagePage.loading,
    error: rootState.messagePage.error,
});

const actions = {
    fetchMessage,
    editMessage,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessagePage);