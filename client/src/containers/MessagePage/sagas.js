import { fetchMessage } from './../../routines';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as messageService from './../../services/messageService';

export function* getMessage(action) {
    try {
        yield put(fetchMessage.request());
        const message = yield call(messageService.getMessage, action.payload.id);

        yield put(fetchMessage.success(message));
    } catch (error) {
        yield put(fetchMessage.failure(error.message));
    } finally {
        yield put(fetchMessage.fulfill());
    }
}

function* watchGetMessage() {
    yield takeEvery(fetchMessage.TRIGGER, getMessage)
}

export default function* messageSaga() {
    yield all([
        watchGetMessage(),
    ])
};
