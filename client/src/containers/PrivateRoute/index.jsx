/* eslint-disable react/forbid-prop-types */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const PrivateRoute = ({ isAuthorized, location, adminRoute, currentUser, ...props }) => (
    (isAuthorized && (adminRoute ? currentUser.isAdmin : true))
        ? <Route {...props} render={({ Component, ...rest }) => <Component {...rest} />} />
        : <Redirect to={{ pathname: '/login', state: { from: location } }} />
);

PrivateRoute.propTypes = {
    isAuthorized: PropTypes.bool,
    adminRoute: PropTypes.bool,
    location: PropTypes.any,
    currentUser: PropTypes.object,
};

PrivateRoute.defaultProps = {
    isAuthorized: false,
    location: undefined,
    adminRoute: false,
};

const mapStateToProps = rootState => ({
    isAuthorized: rootState.auth.isAuthorized,
    loading: rootState.auth.loading,
    currentUser: rootState.auth.user,
});

export default connect(mapStateToProps)(PrivateRoute);
