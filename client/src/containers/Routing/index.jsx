import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Chat from './../Chat';
import AdminPage from './../AdminPage';
import Login from './../../components/Login';
import MessagePage from './../MessagePage';
import UserPage from './../UserPage';
import Spinner from './../../components/Spinner';
import NotFound from './../../scenes/NotFound';
import PrivateRoute from './../../containers/PrivateRoute';
import { authUser } from './../../routines';
import { loginUser } from './actions';
import PropTypes from 'prop-types';

class Routing extends React.Component {
    componentDidMount() {
        this.props.authUser();
    }

    renderLogin = loginProps => (
        <Login
            {...loginProps}
            isAuthorized={this.props.isAuthorized}
            login={this.props.loginUser}
            loading={this.props.loading}
            isAdmin={this.props.user ? this.props.user.isAdmin : false}
        />
    );

    render() {
        const { loading } = this.props;
        return (
            loading
                ? <Spinner />
                : (
                    <div className="fill">
                        <main className="fill">
                            <Switch>
                                <Route exact path="/login" render={this.renderLogin} />
                                <PrivateRoute exact path={"/users"} adminRoute={true} component={AdminPage} />
                                <PrivateRoute exact path="/" component={Chat} />
                                <PrivateRoute path="/message/:id" component={MessagePage}/>
                                <PrivateRoute path="/user/:id" adminRoute={true} component={UserPage}/>
                                <Route path="*" exact component={NotFound} />
                            </Switch>
                        </main>
                    </div>
                )
        );
    }
}

Routing.propTypes = {
    isAuthorized: PropTypes.bool,
    loginUser: PropTypes.func.isRequired,
    user: PropTypes.objectOf(PropTypes.any),
    loading: PropTypes.bool,
    authUser: PropTypes.func.isRequired,
    error: PropTypes.string,
};

Routing.defaultProps = {
    isAuthorized: false,
    user: {},
    loading: true,
    userId: undefined
};

const actions = { authUser, loginUser };

const mapStateToProps = rootState => ({
    isAuthorized: rootState.auth.isAuthorized,
    user: rootState.auth.user,
    loading: rootState.auth.loading,
    error: rootState.auth.error,
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Routing);
