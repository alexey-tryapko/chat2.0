import { authUser } from './../../routines';

const initialState = {
  user: null,
  loading: false,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case authUser.TRIGGER:
      return {
        ...state,
        loading: true,
      };
    case authUser.SUCCESS:
      return {
        ...state,
        user: action.payload,
        isAuthorized: Boolean(action.payload && action.payload.id)
      };
    case authUser.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case authUser.FULFILL:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
}