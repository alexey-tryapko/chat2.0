import { authUser } from './../../routines';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as authService from './../../services/authService';
import { LOGIN_USER } from './actionTypes';

export function* setCurrentUser() {
    try {
        yield put(authUser.request());
        const user = yield call(authService.getCurrentUser);

        yield put(authUser.success(user));
    } catch (error) {
        yield put(authUser.failure(error.message));
    } finally {
        yield put(authUser.fulfill());
    }
}

function* watchSetCurrentUser() {
    yield takeEvery(authUser.TRIGGER, setCurrentUser)
}

export function* loginUser(action) {
    try {
        const { token } = yield call(authService.login, action.payload.user);
        localStorage.setItem('token', token);
        yield put(authUser());
    } catch (error) {
        console.log(error);
    }
}

function* watchLoginUser() {
    yield takeEvery(LOGIN_USER, loginUser)
}

export default function* authSaga() {
    yield all([
        watchLoginUser(),
        watchSetCurrentUser(),
    ])
};
