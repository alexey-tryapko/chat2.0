import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import EditUser from '../../components/EditUser';
import Spinner from './../../components/Spinner';
import { editUser, addUser } from './../AdminPage/actions';
import { fetchUser } from './../../routines';

class UserPage extends React.Component {
    static defaultUserConfig = {
        name: '',
        email: '',
        avatar: '',
        password: '',
    }
    componentDidMount() {
        if (this.props.match.params.id !== 'new') {
            this.props.fetchUser({ id: this.props.match.params.id })
        }
    }

    render() {
        const { user, loading, editUser, history, addUser } = this.props;
        return (
            <div>
                {
                    loading
                        ? <Spinner />
                        : (
                            this.props.match.params.id === 'new' ? (
                                <EditUser
                                    user={UserPage.defaultUserConfig}
                                    editUser={addUser}
                                    history={history}
                                />) : (
                                    user.id && (
                                        <EditUser
                                            user={user}
                                            editUser={editUser}
                                            history={history}
                                        />
                                    )
                                )
                                
                        )
                }
            </div>
        );
    }
}

UserPage.propTypes = {
    user: PropTypes.objectOf(PropTypes.any),
    loading: PropTypes.bool.isRequired,
    error: PropTypes.string,
    editUser: PropTypes.func.isRequired,
    addUser: PropTypes.func.isRequired,
    fetchUser: PropTypes.func.isRequired,
};

UserPage.defaultProps = {
    user: {
        name: '',
        email: '',
        avatar: '',
        password: '',
    },
    loading: true,
};

const mapStateToProps = rootState => ({
    user: rootState.userPage.user,
    loading: rootState.userPage.loading,
    error: rootState.userPage.error,
});

const actions = {
    fetchUser,
    editUser,
    addUser,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserPage);