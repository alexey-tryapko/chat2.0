import { fetchUser } from './../../routines';

const initialState = {
  user: {
    name: '',
    email: '',
    avatar: '',
    password: '',
  },
  loading: false,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case fetchUser.TRIGGER:
      return {
        ...state,
        loading: true,
      };
    case fetchUser.SUCCESS:
      return {
        ...state,
        user: action.payload,
      };
    case fetchUser.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case fetchUser.FULFILL:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
}