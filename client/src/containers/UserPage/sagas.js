import { fetchUser } from './../../routines';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as userService from './../../services/userService';

export function* getUser(action) {
    try {
        yield put(fetchUser.request());
        const user = yield call(userService.getUser, action.payload.id);

        yield put(fetchUser.success(user));
    } catch (error) {
        yield put(fetchUser.failure(error.message));
    } finally {
        yield put(fetchUser.fulfill());
    }
}

function* watchGetUser() {
    yield takeEvery(fetchUser.TRIGGER, getUser)
}

export default function* userSaga() {
    yield all([
        watchGetUser(),
    ])
};
