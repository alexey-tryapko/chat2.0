import { createRoutine } from 'redux-saga-routines';

export const fetchMessages = createRoutine('FETCH_MESSAGES');
export const fetchMessage = createRoutine('FETCH_MESSAGE');
export const fetchUsers = createRoutine('FETCH_USERS');
export const fetchUser = createRoutine('FETCH_USER');
export const authUser = createRoutine('AUTH_USER');