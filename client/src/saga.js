import { all } from 'redux-saga/effects';
import chatSaga from './containers/Chat/sagas';
import messageSaga from './containers/MessagePage/sagas';
import authSaga from './containers/Routing/sagas';
import usersSaga from './containers/AdminPage/sagas';
import userSaga from './containers/UserPage/sagas';
export default function* rootSaga() {
    yield all([
        chatSaga(),
        messageSaga(),
        authSaga(),
        usersSaga(),
        userSaga(),
    ])
};