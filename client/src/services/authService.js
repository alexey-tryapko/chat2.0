import callWebApi from './../helpers/webApiHelper';

export const login = async user => {
    const request = { ...user };
    const response = await callWebApi({
        endpoint: 'http://localhost:3000/api/auth/login',
        type: 'POST',
        request,
    });
    return response.json();
};

export const getCurrentUser = async () => {
    const response = await callWebApi({
        endpoint: 'http://localhost:3000/api/auth/user',
        type: 'GET',
    });
    return response.json();
};