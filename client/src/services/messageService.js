import callWebApi from './../helpers/webApiHelper';
import moment from 'moment';

export const getAllMessages = async () => {
    const response = await callWebApi({
        endpoint: 'http://localhost:3000/api/message/',
        type: 'GET',
    });
    return response.json();
};

export const getMessage = async (id) => {
    const response = await callWebApi({
        endpoint: `http://localhost:3000/api/message/${id}`,
        type: 'GET',
    });
    return response.json();
}

export const addMessage = async (body, user, avatar) => {
    const request = {
        user,
        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        message: body,
        avatar,
    }
    const response = await callWebApi({
        endpoint: `http://localhost:3000/api/message/`,
        type: 'POST',
        request,
    })
    return response.json();
};

export const editMessage = async request => {
    const { id } = request;
    const response = await callWebApi({
        endpoint: `http://localhost:3000/api/message/${id}`,
        type: 'PUT',
        request,
    })
    return response.json();
};

export const likeMessage = async (id, userID) => {
    const message = await getMessage(id);
    let { likes } = message;
    if (likes) {
        const index = likes.indexOf(userID);
        if (index !== -1) {
            likes = likes.filter(id => id !== userID);
        } else {
            likes.push(userID);
        }
    } else {
        likes = [userID];
    }
    message.likes = likes;
    return await editMessage(message);
};

export const deleteMessage = async id => {
    const response = await callWebApi({
        endpoint: `http://localhost:3000/api/message/${id}`,
        type: 'DELETE',
    });
    return response.json();
};


