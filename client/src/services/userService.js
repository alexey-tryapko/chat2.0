import callWebApi from './../helpers/webApiHelper';

export const getAllUsers = async () => {
    const response = await callWebApi({
        endpoint: 'http://localhost:3000/api/user/',
        type: 'GET',
    });
    return response.json();
};

export const getUser = async (id) => {
    const response = await callWebApi({
        endpoint: `http://localhost:3000/api/user/${id}`,
        type: 'GET',
    });
    return response.json();
};

export const addUser = async (name, email, password, avatar) => {
    const request = {
        name,
        email,
        password,
        avatar,
    }
    const response = await callWebApi({
        endpoint: `http://localhost:3000/api/user/`,
        type: 'POST',
        request,
    })
    return response.json();
};

export const editUser = async request => {
    const { id } = request;
    const response = await callWebApi({
        endpoint: `http://localhost:3000/api/user/${id}`,
        type: 'PUT',
        request,
    })
    return response.json();
};

export const deleteUser = async id => {
    const response = await callWebApi({
        endpoint: `http://localhost:3000/api/user/${id}`,
        type: 'DELETE',
    });
    return response.json();
};