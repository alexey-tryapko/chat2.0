import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware } from 'connected-react-router';

import chatReducer from './containers/Chat/reducer';
import messageReducer from './containers/MessagePage/reducer';
import authReducer from './containers/Routing/reducer';
import usersReducer from './containers/AdminPage/reducer';
import userReducer from './containers/UserPage/reducer';

import rootSaga from './saga';

const initialState = {
    chat: {
        chatName: 'Friends',
    }
};

const reducers = {
    chat: chatReducer,
    messagePage: messageReducer,
    userPage: userReducer,
    auth: authReducer,
    adminPage: usersReducer, 
};

const sagaMiddleware = createSagaMiddleware();
export const history = createBrowserHistory();

const middlewares = [
    sagaMiddleware,
    routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    router: connectRouter(history),
    ...reducers
})

const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(
        applyMiddleware(...middlewares)
    )
);

export default store;

sagaMiddleware.run(rootSaga);