import { Router } from 'express';
import * as messageService from '../services/message.service';

const router = Router();

router
    .get('/', (req, res, next) => messageService.getAllMessages()
        .then(messages => res.send(messages))
        .catch(next))
    .get('/:id', (req, res, next) => messageService.getMessageById(req.params.id)
        .then(message => res.send(message))
        .catch(next))
    .post('/', (req, res, next) => messageService.addMessage(req.body)
        .then(message => res.send(message))
        .catch(next))
    .put('/:id', (req, res, next) => messageService.updateMessage(req.params.id, req.body)
        .then(message => res.send(message))
        .catch(next))
    .delete('/:id', (req, res, next) => messageService.deleteMessage(req.params.id)
        .then(result => res.send(result))
        .catch(next))

export default router;
