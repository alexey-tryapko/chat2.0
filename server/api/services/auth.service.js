import tokenHelper from '../../helpers/token.helper';
import userRepository from '../../data/repositories/user.repository';

export const login = async ({ id }) => ({
    token: tokenHelper.createToken({ id }),
    user: await userRepository.getUserById(id)
});
