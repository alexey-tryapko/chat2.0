import messageRepository from '../../data/repositories/message.repository';

export const getAllMessages = async () => {
    const messages = await messageRepository.getAll();
    return messages;
};

export const getMessageById = async id => {
    const message = await messageRepository.getById(id);
    return message;
};

export const addMessage = async data => {
    const res = await messageRepository.addMessage(data);
    return res;
};

export const updateMessage = async (id, message) => {
    const res = await messageRepository.updateById(id, message);
    return res;
};

export const deleteMessage = async id => {
    const res = await messageRepository.deleteById(id);
    return res;
};