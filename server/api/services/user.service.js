import userRepository from '../../data/repositories/user.repository';

export const getUserById = async (userId) => {
    const res = await userRepository.getUserById(userId);
    return res;
};

export const getAllUsers = async () => {
    const users = await userRepository.getAll();
    return users;
};

export const addUser = async data => {
    const res = await userRepository.addUser(data);
    return res;
};

export const updateUser = async (id, user) => {
    const res = await userRepository.updateById(id, user);
    return res;
};

export const deleteUser = async id => {
    const res = await userRepository.deleteById(id);
    return res;
};