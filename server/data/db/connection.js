import * as admin from 'firebase-admin';
import config from '../../config/db.config';

admin.initializeApp({
    credential: admin.credential.cert(config)
});

export const db = admin.firestore();

