import { db } from "./../db/connection";

export default class BaseRepository {
    constructor(collection) {
        this.collection = collection;
    }

    generateId() {
        return '' + Math.random().toString(36).substr(2, 9)
    };

    getAll() {
        return db.collection(this.collection).get()
            .then(snapshot => {
                const res = [];
                snapshot.forEach(doc => res.push(doc.data()));
                return res;
            })
            .catch((err) => {
                console.log('Error getting documents', err);
            });
    }

    getById(id) {
        return db.collection(this.collection).doc(id).get()
            .then(doc => {
                if (!doc.exists) {
                    console.log('No such document!');
                } else {
                    return doc.data();
                }
            })
            .catch(err => {
                console.log('Error getting document', err);
            });
    }

    create(data) {
        return db.collection(this.collection).doc(data.id).set(data);
    }

    updateById(id, data) {
        return db.collection(this.collection).doc(id).update(data);
    }

    deleteById(id) {
        return db.collection(this.collection).doc(id).delete();
    }
}
