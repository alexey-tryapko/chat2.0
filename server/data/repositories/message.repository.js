import BaseRepository from './base.repository';

class MessageRepository extends BaseRepository {
    constructor() {
        super('messages');
    }

    getAllMessages() {
        return this.getAll();
    }

    addMessage(message) {
        message.id = this.generateId();
        return this.create(message);
    }

    deleteMessage(id) {
        return this.deleteById(id);
    }
}

export default new MessageRepository();
