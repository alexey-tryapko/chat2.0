import BaseRepository from './base.repository';

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }
    
    addUser(user) {
        user.id = this.generateId();
        return this.create(user);
    }

    async getByEmail(email) {
        const users = await this.getAll();
        const res = users.filter(user => user.email === email);
        return res[0];
    }

    getUserById(id) {
        return this.getById(id);    
    }

    deleteUser(id) {
        return this.deleteById(id);
    }
}

export default new UserRepository();
